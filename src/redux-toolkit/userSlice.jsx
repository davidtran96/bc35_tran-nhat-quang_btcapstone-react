const { createSlice } = require("@reduxjs/toolkit");
const { userLocalService } = require("../service/localService");

const initialState = {
  user: userLocalService.get(),
};
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfo: (state, action) => {
      state.user = action.payload;
    },
  },
});

export const { setUserInfo } = userSlice.actions;
export default userSlice.reducer;
