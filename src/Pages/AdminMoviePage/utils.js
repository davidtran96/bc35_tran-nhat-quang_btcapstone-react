import { Tag } from "antd";

export const movieColums = [
  {
    title: "Tên phim",
    dataIndex: "tenPhim",
    key: "tenPhim",
  },
  {
    title: "Bí Danh",
    dataIndex: "biDanh",
    key: "biDanh",
  },
  {
    title: "Trailer",
    dataIndex: "trailer",
    key: "trailer",
  },
  {
    title: "Mô tả",
    dataIndex: "moTa",
    key: "moTa",
  },

  {
    title: "Hành động",
    dataIndex: "action",
    key: "action",
  },
];
