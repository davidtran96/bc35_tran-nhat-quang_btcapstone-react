import { Button, message, Table } from "antd";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import withAuth from "../../HOC/withAuth";
import { deletMovvie, getMovieList } from "../../service/movieService";
import { movieColums } from "./utils";

function AdminMoviePage() {
  const [movieArr, setMovieArr] = useState([]);

  useEffect(() => {
    let handleRemoveMovie = (movice) => {
      deletMovvie(movice)
        .then((result) => {
          console.log(result);
          message.success("Xoá thành công");
        })
        .catch((err) => {
          console.log(err);
          message.error("Xoá thất bại");
        });
    };
    let fectMovieList = () => {
      getMovieList()
        .then((result) => {
          let movieList = result.data.content.map((item) => {
            return {
              ...item,
              action: (
                <div className="space-x-3">
                  <Button
                    type="primary"
                    danger
                    onClick={() => {
                      handleRemoveMovie(item.MaPhim);
                    }}
                  >
                    Xoá
                  </Button>
                  <Button type="dashed">Sửa</Button>
                </div>
              ),
            };
          });
          setMovieArr(movieList);
          console.log(result);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fectMovieList();
  }, []);
  return (
    <div>
      <Table columns={movieColums} dataSource={movieArr} />
    </div>
  );
}
export default withAuth(AdminMoviePage);
