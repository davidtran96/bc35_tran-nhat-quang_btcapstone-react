import { Tag } from "antd";

export const userColums = [
  {
    title: "Tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "emailmaLoaiNguoiDung",
    render: (text) => {
      text == "QuanTri" ? (
        <Tag color="red">Admin</Tag>
      ) : (
        <Tag color="blue">User</Tag>
      );
    },
  },
  {
    title: "Hành động",
    dataIndex: "action",
    key: "action",
  },
];
