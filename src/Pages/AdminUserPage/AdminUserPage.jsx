import { Button, message, Table } from "antd";
import React, { useEffect, useState } from "react";
import withAuth from "../../HOC/withAuth";
import { deleteUser, getUserList } from "../../service/adminService";
import { userColums } from "./utils";

function AdminUserPage() {
  const [userArr, setUserArr] = useState([]);

  useEffect(() => {
    let handleRemoveUser = (acount) => {
      deleteUser(acount)
        .then((res) => {
          console.log(res);
          message.success("Xoá thành công");
          fetchUserList();
        })
        .catch((err) => {
          console.log(err);
          message.error("Xoá thất bại");
        });
    };
    let fetchUserList = () => {
      getUserList()
        .then((res) => {
          let userList = res.data.content.map((item) => {
            return {
              ...item,
              action: (
                <div className="space-x-5">
                  <Button
                    type="primary"
                    danger
                    onClick={() => {
                      handleRemoveUser(item.taiKhoan);
                    }}
                  >
                    Xoá
                  </Button>
                  <Button type="dashed">Sửa</Button>
                </div>
              ),
            };
          });
          setUserArr(userList);
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchUserList();
  }, []);
  return (
    <div>
      <Table columns={userColums} dataSource={userArr} />
    </div>
  );
}
export default withAuth(AdminUserPage);
