import { https } from "./configURL";

export const getMovieList = () => {
  return https.get(`/api/QuanLyPhim/LayDanhSachPhim?MaNhom=GP01`);
};

export const getMovieByTheater = () => {
  return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
};

export const deletMovvie = (movie) => {
  return https.delete(
    `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/XoaPhim=${movie}`
  );
};
