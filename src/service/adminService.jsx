import { https } from "./configURL";

export const getUserList = () => {
  return https.get(`/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01`);
};
export const deleteUser = (account) => {
  return https.delete(
    `https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${account}`
  );
};
