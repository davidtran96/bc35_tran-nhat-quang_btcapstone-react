import React, { useEffect, useState } from "react";

export default function Footer() {
  return (
    <footer className="text-gray-500 bg-slate-900 w-full ">
      <div className="container mx-auto py-5 ">
        <div className="flex justify-between items-start text-base mobile:grid mobile:grid-cols-2">
          <div className="w-1/4 mobile:text-xs mobile:w-1/2">
            <h1 className="text-red-500 text-3xl mobile:text-xl font-bold  mobile:p-1 hover:text-red-700">
              Movies
            </h1>
            <div className="flex justify-around text-left items-center font-semibold ">
              <ul>
                <li className="p-2 hover:text-white">FAQ</li>
                <li className="p-2 hover:text-white">Brand Guidelines</li>
                <li className="p-2 hover:text-white">Thỏa thuận sử dụng</li>
                <li className="p-2 hover:text-white">Chính sách bảo mật</li>
              </ul>
            </div>
          </div>

          <div className="w-1/4 ">
            <h1 className="text-xl text-white text-left font-bold text-transform: capitalize p-3">
              Mobile app
            </h1>
            <ul className="flex flex-col justify-center items-start font-semibold p-1">
              <li className="p-2 space-x-2 hover:text-white text-2xl flex justify-center items-center">
                <i className="fab fa-app-store"></i>
                <span className="text-base">App store</span>
              </li>
              <li className="p-2 space-x-2 hover:text-white text-2xl flex justify-center items-center">
                <i className="fab fa-android"></i>
                <span className="text-base">CH Play</span>
              </li>
            </ul>
          </div>
          <div className="w-1/4 ">
            <h1 className="text-xl text-white text-left font-bold text-transform: capitalize p-3">
              Social
            </h1>
            <ul className="flex flex-col justify-center items-start font-semibold p-1">
              <li className="p-2 space-x-2 hover:text-white text-2xl flex justify-center items-center">
                <i className="fab fa-facebook"></i>
                <span className="text-base">Facebook</span>
              </li>
              <li className="p-2 space-x-2 hover:text-white text-2xl flex justify-center items-center">
                <i className="fab fa-instagram"></i>
                <span className="text-base">Instagram</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
}
