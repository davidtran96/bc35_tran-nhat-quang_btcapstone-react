import React from "react";
import { useSelector } from "react-redux";
import { RiseLoader } from "react-spinners";

export default function Spinner() {
  let isLoading = useSelector((state) => {
    return state.spinnerSlice.isLoading;
  });
  return isLoading ? (
    <div className="fixed bg-black w-screen h-screen top-0 left-0 z-50 flex justify-center items-center">
      <RiseLoader size="20" speedMultiplier={2} color="#FFB100" />
    </div>
  ) : (
    <></>
  );
}
