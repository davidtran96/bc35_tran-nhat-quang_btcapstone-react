import logo from "./logo.svg";
import "./App.css";
import Spinner from "./Components/Spinner/Spinner";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import AdminUserPage from "./Pages/AdminUserPage/AdminUserPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import AdminMoviePage from "./Pages/AdminMoviePage/AdminMoviePage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import Main from "./Layout/Main";

function App() {
  return (
    <div>
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />

          <Route path="/admin/user" element={<AdminUserPage />} />
          <Route path="/admin/movie" element={<AdminMoviePage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
